# ModelArts-Lab

This library is an AI developer communication and learning platform for ModelArts. All cases and sample code are designed based on the industry-leading AI development platform ModelArts (huawei cloud), helping AI developers to quickly master the artificial intelligence skills.

Visit count: [![HitCount](http://hits.dwyl.io/chenliang613/huaweicloud/ModelArts-Lab.svg)](http://hits.dwyl.io/chenliang613/huaweicloud/ModelArts-Lab)

## Online Documents
* [MoXing-API](docs/moxing_api_doc)
* [FAQs](docs/faqs.md) 

## Introduction of Cases

### ExeML Cases

In order to improve the Industry AI process   , AI must reduce the difficulty and threshold of AI model development. At present, only a few algorithm engineers and researchers master the development and tuning capabilities of AI, and most algorithm engineers only master the algorithm prototyping capabilities, lacking the relevant prototype to real product and engineering capabilities. For most developers, the development and parameter tuning capabilities of the AI algorithm are not available. This has resulted in most companies not having AI development capabilities.

ModelArts ExeML to enable zero AI-based  developers to quickly complete training and deployment of models. According to the annotation data provided by the developer and the selected scenario, the model that meets the user's accuracy requirements is automatically generated without any code development. Supports image classification, object detection, predictive analysis, and sound classification scenes. The model can be automatically tuned and generated to meet the requirements based on the speed of the final deployment environment and the speed of the developer's needs.


### Notebook Cases

ModelArts integrates Jupyter Notebook(an open source tools) which provide an interactive development tools for AI developers on browser. After creating a development environment, developers can write and run model training code by themselves, and then train the model based on the code.

The ModelArts Notebook development environment is ideally suited as a tool for teaching and learning artificial intelligence. There are many well-known educational institutions that offer artificial intelligence courses based on ModelArts.


### Train_inference Cases

End-to-end data development, model development, model training, model deployment and release, model sharing (AI market) and other full-process artificial intelligence model development, and application practices In ModelArts.